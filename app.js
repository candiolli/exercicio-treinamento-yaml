const fs = require('fs');
const YAML = require('yaml')

const file = fs.readFileSync('./file1.yaml', 'utf8')
const doc = YAML.parse(file);
console.log("# Texto com quebra de linha")
console.log(doc.lista)

const file2 = fs.readFileSync('./file2.yaml', 'utf8')
const doc2 = YAML.parseAllDocuments(file2);
console.log("# FILE 2")
console.log(doc2[0].contents.items)

// const file3 = fs.readFileSync('./file3.yaml', 'utf8')
// const doc3 = YAML.parse(file3);
// console.log("# FILE 3")
// console.log(doc3)       